#!/usr/bin/env python
# coding: utf-8

# In[2]:


from keras_applications.imagenet_utils import _obtain_input_shape
from keras import backend as K
from keras.layers import Input, Convolution2D, MaxPooling2D, Activation, concatenate, Dropout
from keras.layers import GlobalAveragePooling2D, GlobalMaxPooling2D
from keras.models import Model
from keras.engine.topology import get_source_inputs
from keras.utils import get_file
from keras.utils import layer_utils


sq1x1 = "squeeze1x1"
exp1x1 = "expand1x1"
exp3x3 = "expand3x3"
relu = "relu_"

WEIGHTS_PATH = "https://github.com/rcmalli/keras-squeezenet/releases/download/v1.0/squeezenet_weights_tf_dim_ordering_tf_kernels.h5"
WEIGHTS_PATH_NO_TOP = "https://github.com/rcmalli/keras-squeezenet/releases/download/v1.0/squeezenet_weights_tf_dim_ordering_tf_kernels_notop.h5"

# Modular function for Fire Node

def fire_module(x, fire_id, squeeze=16, expand=64):
    s_id = 'fire' + str(fire_id) + '/'

    if K.image_data_format() == 'channels_first':
        channel_axis = 1
    else:
        channel_axis = 3

    x = Convolution2D(squeeze, (1, 1), padding='valid', name=s_id + sq1x1)(x)
    x = Activation('relu', name=s_id + relu + sq1x1)(x)

    left = Convolution2D(expand, (1, 1), padding='valid', name=s_id + exp1x1)(x)
    left = Activation('relu', name=s_id + relu + exp1x1)(left)

    right = Convolution2D(expand, (3, 3), padding='same', name=s_id + exp3x3)(x)
    right = Activation('relu', name=s_id + relu + exp3x3)(right)

    x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')
    return x


# Original SqueezeNet from paper.

def SqueezeNet():

    classes = 1000
    input_tensor = None
    input_shape = _obtain_input_shape(input_tensor,
                                      default_size=227,
                                      min_size=48,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top)

    if not K.is_keras_tensor(input_tensor):
        img_input = Input(tensor=input_tensor, shape=input_shape)
    else:
        img_input = input_tensor


    x = Convolution2D(64, (3, 3), strides=(2, 2), padding='valid', name='conv1')(img_input)
    x = Activation('relu', name='relu_conv1')(x)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool1')(x)

    x = fire_module(x, fire_id=2, squeeze=16, expand=64)
    x = fire_module(x, fire_id=3, squeeze=16, expand=64)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool3')(x)

    x = fire_module(x, fire_id=4, squeeze=32, expand=128)
    x = fire_module(x, fire_id=5, squeeze=32, expand=128)
    x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool5')(x)

    x = fire_module(x, fire_id=6, squeeze=48, expand=192)
    x = fire_module(x, fire_id=7, squeeze=48, expand=192)
    x = fire_module(x, fire_id=8, squeeze=64, expand=256)
    x = fire_module(x, fire_id=9, squeeze=64, expand=256)

    x = Dropout(0.5, name='drop9')(x)

    x = Convolution2D(classes, (1, 1), padding='valid', name='conv10')(x)
    x = Activation('relu', name='relu_conv10')(x)
    x = GlobalAveragePooling2D()(x)
    x = Activation('softmax', name='loss')(x)

    inputs = img_input

    model = Model(inputs, x, name='squeezenet')

    # load weights
    weights_path = get_file('squeezenet_weights_tf_dim_ordering_tf_kernels.h5',
                            WEIGHTS_PATH,
                            cache_subdir='models')

    model.load_weights(weights_path)

    return model


# In[ ]:


import numpy as np
from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.preprocessing import image
import os

model = SqueezeNet()


with open('ILSVRC2012_validation_ground_truth.txt') as gt:
    y = gt.read().splitlines()

map = {}
with open('map_clsloc.txt') as f:
    for line in f:
        arr = line.split(' ')
        map[arr[0]]=arr[1]


dataDir = "SqueezeNet/ILSVRC2012_img_val/"
right_1x = 0
right_5x = 0
counter = 0
output = open("ValidationLog.csv", "w")
output.write("Id,Truth,Score1,Score5,Predicted1,Predicted2,Predicted3,Predicted4,Predicted5" + "\n")
print("Validating...")
for filename in os.listdir(dataDir):
    counter+=1
    r1 = 0
    r5 = 0
    id = int(filename.replace(".JPEG","").replace("ILSVRC2012_val_",""))
    img = image.load_img(os.path.join(dataDir, filename), target_size=(227, 227))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    preds = model.predict(x)
    decoded = decode_predictions(preds)

    for i in range(5):
        if (map[decoded[0][i][0]] == y[id-1]):

            right_5x+=1
            r5=1
            if (i == 0):
                right_1x+=1
                r1=1
    if (counter % 1000 == 0):
        v1x = right_1x/counter
        v5x = right_5x/counter
        print("\tValidating the "+str(counter)+"th example (1x: "+str(v1x)+", 5x: "+str(v5x)+")")
    output.write(str(id) + "," + str(y[id-1])+ "," + str(r1) + "," + str(r5) + "," + str(map[decoded[0][0][0]])+ "," + str(map[decoded[0][1][0]])+ "," + str(map[decoded[0][2][0]])+ "," + str(map[decoded[0][3][0]])+ "," + str(map[decoded[0][4][0]])+"\n")

v1x = right_1x/counter
v5x = right_5x/counter
print("Validation Accuracy (1x): "+str(v1x))
print("Validation Accuracy (5x): "+str(v5x))
